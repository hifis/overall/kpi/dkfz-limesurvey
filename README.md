# Service Usage

* DKFZ LimeSurvey

## Plotting

* Plotting is be performed in the [Plotting project](https://gitlab.hzdr.de/hifis/overall/kpi/kpi-plots-ci).

## Data + Weighing

./stats/usage-stats-dkfz-limesurvey-weekly.csv - schedule: weekly

| Data | Weighing | Unit | Comment |
| ----- | ----- | ----- | ----- |
| total surveys | 16,667% | Quantity/Number | Nr of all surveys |
| active surveys | 16,667% | Quantity/Number | Nr of active surveys |
| users | 16,667% | Quantity/Number| Nr of partaking users of active surveys |
| survey admins | 16,667% | Quantity/Number| Nr of users creating and administrating surveys |
| survey admin groups | 0% | Quantity/Number | Nr of groups of survey admins |
| unique institutes | 16,667% | Quantity/Number | Nr of unique institutes |
| total questions of active surveys | 16,667% | Quantity/Number | Sum of questions in all active surveys |


## Schedule

* weekly
