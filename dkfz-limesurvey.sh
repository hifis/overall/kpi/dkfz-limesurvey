#!/bin/bash

DB_NAME=$LIMESURVEY_DB_NAME
DB_USER=$LIMESURVEY_DB_USER
DB_PASS=$LIMESURVEY_DB_PASS
DATETIME=$(date --rfc-3339=seconds)

TOTAL_SURVEYS=$(mysql --user=$DB_USER --password=$DB_PASS --database=$DB_NAME -s -N --execute="SELECT COUNT(sid) AS total_surveys FROM lime_surveys;")

ACTIVE_SURVEYS=$(mysql --user=$DB_USER --password=$DB_PASS --database=$DB_NAME -s -N --execute="SELECT COUNT(sid) AS active_surveys FROM lime_surveys WHERE active='Y';")

NUM_USERS=0
mapfile -t TEMP_SIDS < <( mysql --user=$DB_USER --password=$DB_PASS --database=$DB_NAME -s -N --execute="SELECT sid AS survey_responses FROM lime_surveys WHERE active='Y';" )
for TEMP_SID in "${TEMP_SIDS[@]}"; do
  TEMP_SID_USERS=$( mysql --user=$DB_USER --password=$DB_PASS --database=$DB_NAME -s -N --execute="SELECT COUNT(id) FROM lime_survey_${TEMP_SID} WHERE submitdate IS NOT NULL;" )
  NUM_USERS=$(( NUM_USERS + TEMP_SID_USERS ))
done

NUM_ADMINS=$(mysql --user=$DB_USER --password=$DB_PASS --database=$DB_NAME -s -N --execute="SELECT COUNT(uid) AS num_users FROM lime_users WHERE users_name <> 'admin';")

NUM_ADMIN_GROUPS=$(mysql --user=$DB_USER --password=$DB_PASS --database=$DB_NAME -s -N --execute="SELECT COUNT(ugid) AS num_user_groups FROM lime_user_groups WHERE name <> 'Unauthorized Users';")

UNIQUE_INSTITUTES=$(mysql --user=$DB_USER --password=$DB_PASS --database=$DB_NAME -s -N --execute="SELECT COUNT(DISTINCT SUBSTRING_INDEX(email, '@', -1)) AS unique_institutes FROM lime_users WHERE users_name <> 'admin';")

TOTAL_QUESTIONS_ACTIVE_SURVEYS=$(mysql --user=$DB_USER --password=$DB_PASS --database=$DB_NAME -s -N --execute="SELECT count(qid) as TOTAL_QUESTIONS_ACTIVE_SURVEYS from lime_questions where sid in (SELECT sid FROM lime_surveys WHERE active='Y');")

echo $DATETIME,$TOTAL_SURVEYS,$ACTIVE_SURVEYS,$NUM_USERS,$NUM_ADMINS,$NUM_ADMIN_GROUPS,$UNIQUE_INSTITUTES,$TOTAL_QUESTIONS_ACTIVE_SURVEYS, >> ./stats/usage-stats-dkfz-limesurvey-weekly.csv
